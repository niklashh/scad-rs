use std::io::BufRead;

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char, digit1, multispace0, multispace1, one_of},
    combinator::{cut, map, map_res, opt},
    error::{context, VerboseError},
    multi::many0,
    sequence::{delimited, preceded, terminated, tuple},
    IResult, Parser,
};

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum BuiltIn {
    Plus,
    Minus,
    Times,
    Divide,
    Equal,
    Not,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Atom {
    Num(i32),
    Keyword(String),
    Boolean(bool),
    BuiltIn(BuiltIn),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expr {
    Constant(Atom),
    Application(Box<Expr>, Vec<Expr>),
    If(Box<Expr>, Box<Expr>),
    IfElse(Box<Expr>, Box<Expr>, Box<Expr>),
    Quote(Vec<Expr>),
}

fn parse_builtin_op(i: &str) -> IResult<&str, BuiltIn, VerboseError<&str>> {
    let (i, t) = one_of("+-*/=")(i)?;

    Ok((
        i,
        match t {
            '+' => BuiltIn::Plus,
            '-' => BuiltIn::Minus,
            '*' => BuiltIn::Times,
            '/' => BuiltIn::Divide,
            '=' => BuiltIn::Equal,
            _ => unreachable!(),
        },
    ))
}

fn parse_builtin(i: &str) -> IResult<&str, BuiltIn, VerboseError<&str>> {
    alt((parse_builtin_op, map(tag("not"), |_| BuiltIn::Not)))(i)
}

fn parse_bool(i: &str) -> IResult<&str, Atom, VerboseError<&str>> {
    alt((
        map(tag("#f"), |_| Atom::Boolean(false)),
        map(tag("#t"), |_| Atom::Boolean(true)),
    ))(i)
}

fn parse_keyword(i: &str) -> IResult<&str, Atom, VerboseError<&str>> {
    map(
        context("keyword", preceded(tag(":"), cut(alpha1))),
        |sym_str: &str| Atom::Keyword(sym_str.to_string()),
    )(i)
}

fn parse_num(i: &str) -> IResult<&str, Atom, VerboseError<&str>> {
    alt((
        map_res(digit1, |num_str: &str| {
            num_str.parse::<i32>().map(Atom::Num)
        }),
        map(preceded(tag("-"), digit1), |num_str: &str| {
            Atom::Num(-num_str.parse::<i32>().unwrap())
        }),
    ))(i)
}

fn parse_atom(i: &str) -> IResult<&str, Atom, VerboseError<&str>> {
    alt((
        parse_num,
        parse_bool,
        map(parse_builtin, Atom::BuiltIn),
        parse_keyword,
    ))(i)
}

fn parse_constant(i: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(parse_atom, Expr::Constant)(i)
}

fn s_expr<'a, O, F>(inner: F) -> impl FnMut(&'a str) -> IResult<&str, O, VerboseError<&str>>
where
    F: Parser<&'a str, O, VerboseError<&'a str>>,
{
    delimited(
        char('('),
        preceded(multispace0, inner),
        context("closing parens", cut(preceded(multispace0, char(')')))),
    )
}

fn parse_application(i: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    s_expr(map(
        tuple((parse_expr, many0(parse_expr))),
        |(head, tail)| Expr::Application(Box::new(head), tail),
    ))(i)
}

fn parse_if(i: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    s_expr(context(
        "if expression",
        map(
            preceded(
                terminated(tag("if"), multispace1),
                cut(tuple((parse_expr, parse_expr, opt(parse_expr)))),
            ),
            |(pred, true_branch, opt_false_branch)| {
                if let Some(false_branch) = opt_false_branch {
                    Expr::IfElse(
                        Box::new(pred),
                        Box::new(true_branch),
                        Box::new(false_branch),
                    )
                } else {
                    Expr::If(Box::new(pred), Box::new(true_branch))
                }
            },
        ),
    ))(i)
}

fn parse_quote(i: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(
        context("quote", preceded(tag("'"), cut(s_expr(many0(parse_expr))))),
        Expr::Quote,
    )(i)
}

pub fn parse_expr(i: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    preceded(
        multispace0,
        alt((parse_constant, parse_application, parse_if, parse_quote)),
    )(i)
}

#[cfg(test)]
mod test {
    use super::*;
    use BuiltIn::*;
    use Expr::*;

    #[test]
    fn test_parse_expr() {
        let sexpr = Application(
            Box::new(IfElse(
                Box::new(Application(
                    Box::new(Constant(Atom::BuiltIn(Equal))),
                    vec![
                        Application(
                            Box::new(Constant(Atom::BuiltIn(Plus))),
                            vec![
                                Constant(Atom::Num(3)),
                                Application(
                                    Box::new(Constant(Atom::BuiltIn(Divide))),
                                    vec![Constant(Atom::Num(9)), Constant(Atom::Num(3))],
                                ),
                            ],
                        ),
                        Application(
                            Box::new(Constant(Atom::BuiltIn(Times))),
                            vec![Constant(Atom::Num(2)), Constant(Atom::Num(3))],
                        ),
                    ],
                )),
                Box::new(Constant(Atom::BuiltIn(Times))),
                Box::new(Constant(Atom::BuiltIn(Divide))),
            )),
            vec![Constant(Atom::Num(456)), Constant(Atom::Num(123))],
        );

        assert_eq!(
            parse_expr("((if (= (+ 3 (/ 9 3)) (* 2 3)) * /) 456 123)")
                .unwrap()
                .1,
            sexpr
        );
    }
}

pub fn main() {
    loop {
        let mut s = String::with_capacity(128);
        std::io::stdin().lock().read_line(&mut s).unwrap();
        println!("{:#?}", parse_expr(&s).map(|(_, ast)| ast));
    }
}
