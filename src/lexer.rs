use nom::{
    branch::alt,
    bytes::complete::{escaped, is_not, tag, take_while},
    character::complete::{alphanumeric1, anychar, char, digit1, multispace0, one_of, satisfy},
    combinator::{cut, map, map_res, peek, value},
    error::{context, ContextError, ParseError},
    multi::many0,
    sequence::{preceded, terminated},
    IResult, InputIter, InputLength, InputTake, Needed, Slice,
};
use std::{
    iter::Enumerate,
    ops::{Range, RangeFrom, RangeFull, RangeTo},
    slice::Iter,
    str::FromStr,
};

#[derive(PartialEq, Debug, Clone)]
pub enum Token {
    Illegal(char),
    // identifier and literals
    Ident(String),
    StringLiteral(String),
    IntLiteral(i64),
    BoolLiteral(bool),
    // statements
    Assign,
    // operators
    Plus,
    Minus,
    Divide,
    Multiply,
    Equal,
    NotEqual,
    GreaterThanEqual,
    LessThanEqual,
    GreaterThan,
    LessThan,
    Not,
    // reserved words
    // punctuations
    Comma,
    SemiColon,
    LParen,
    RParen,
    LBrace,
    RBrace,
    LBracket,
    RBracket,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Tokens<'a> {
    pub tok: &'a [Token],
    pub start: usize,
    pub end: usize,
}

impl<'a> Tokens<'a> {
    pub fn new(vec: &'a [Token]) -> Self {
        Tokens {
            tok: vec,
            start: 0,
            end: vec.len(),
        }
    }
}

impl<'a> InputLength for Tokens<'a> {
    #[inline]
    fn input_len(&self) -> usize {
        self.tok.len()
    }
}

impl<'a> InputTake for Tokens<'a> {
    #[inline]
    fn take(&self, count: usize) -> Self {
        Tokens {
            tok: &self.tok[0..count],
            start: 0,
            end: count,
        }
    }

    #[inline]
    fn take_split(&self, count: usize) -> (Self, Self) {
        let (prefix, suffix) = self.tok.split_at(count);
        let first = Tokens {
            tok: prefix,
            start: 0,
            end: prefix.len(),
        };
        let second = Tokens {
            tok: suffix,
            start: 0,
            end: suffix.len(),
        };
        (second, first)
    }
}

impl InputLength for Token {
    #[inline]
    fn input_len(&self) -> usize {
        1
    }
}

impl<'a> Slice<Range<usize>> for Tokens<'a> {
    #[inline]
    fn slice(&self, range: Range<usize>) -> Self {
        Tokens {
            tok: self.tok.slice(range.clone()),
            start: self.start + range.start,
            end: self.start + range.end,
        }
    }
}

impl<'a> Slice<RangeTo<usize>> for Tokens<'a> {
    #[inline]
    fn slice(&self, range: RangeTo<usize>) -> Self {
        self.slice(0..range.end)
    }
}

impl<'a> Slice<RangeFrom<usize>> for Tokens<'a> {
    #[inline]
    fn slice(&self, range: RangeFrom<usize>) -> Self {
        self.slice(range.start..self.end - self.start)
    }
}

impl<'a> Slice<RangeFull> for Tokens<'a> {
    #[inline]
    fn slice(&self, _: RangeFull) -> Self {
        Tokens {
            tok: self.tok,
            start: self.start,
            end: self.end,
        }
    }
}

impl<'a> InputIter for Tokens<'a> {
    type Item = &'a Token;
    type Iter = Enumerate<Iter<'a, Token>>;
    type IterElem = Iter<'a, Token>;

    #[inline]
    fn iter_indices(&self) -> Self::Iter {
        self.tok.iter().enumerate()
    }
    #[inline]
    fn iter_elements(&self) -> Self::IterElem {
        self.tok.iter()
    }
    #[inline]
    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        self.tok.iter().position(predicate)
    }
    #[inline]
    fn slice_index(&self, count: usize) -> Result<usize, Needed> {
        if self.tok.len() >= count {
            Ok(count)
        } else {
            Err(Needed::new(count - self.tok.len()))
        }
    }
}

pub fn space<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, &'a str, E> {
    let chars = " \t\r\n";

    take_while(move |c| chars.contains(c))(i)
}

pub fn assign_operator(i: &str) -> IResult<&str, Token> {
    value(Token::Assign, terminated(char('='), peek(is_not("="))))(i)
}

pub fn plus_operator(i: &str) -> IResult<&str, Token> {
    value(Token::Plus, char('+'))(i)
}

pub fn minus_operator(i: &str) -> IResult<&str, Token> {
    value(Token::Minus, char('-'))(i)
}

pub fn divide_operator(i: &str) -> IResult<&str, Token> {
    value(Token::Divide, char('/'))(i)
}

pub fn multiply_operator(i: &str) -> IResult<&str, Token> {
    value(Token::Multiply, char('*'))(i)
}

pub fn equal_operator(i: &str) -> IResult<&str, Token> {
    value(Token::Equal, tag("=="))(i)
}

pub fn not_equal_operator(i: &str) -> IResult<&str, Token> {
    value(Token::NotEqual, tag("!="))(i)
}

pub fn greater_than_equal_operator(i: &str) -> IResult<&str, Token> {
    value(Token::GreaterThanEqual, tag(">="))(i)
}

pub fn less_than_equal_operator(i: &str) -> IResult<&str, Token> {
    value(Token::LessThanEqual, tag("<="))(i)
}

pub fn greater_than_operator(i: &str) -> IResult<&str, Token> {
    value(Token::GreaterThan, terminated(char('>'), peek(is_not("="))))(i)
}

pub fn less_than_operator(i: &str) -> IResult<&str, Token> {
    value(Token::LessThan, terminated(char('<'), peek(is_not("="))))(i)
}

pub fn not_operator(i: &str) -> IResult<&str, Token> {
    value(Token::Not, terminated(char('!'), peek(is_not("="))))(i)
}

pub fn lex_operator(i: &str) -> IResult<&str, Token> {
    alt((
        assign_operator,
        plus_operator,
        minus_operator,
        divide_operator,
        multiply_operator,
        equal_operator,
        not_equal_operator,
        greater_than_equal_operator,
        less_than_equal_operator,
        greater_than_operator,
        less_than_operator,
        not_operator,
    ))(i)
}

pub fn comma_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::Comma, char(','))(i)
}

pub fn semicolon_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::SemiColon, char(';'))(i)
}

pub fn lparen_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::LParen, char('('))(i)
}

pub fn rparen_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::RParen, char(')'))(i)
}

pub fn lbrace_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::LBrace, char('{'))(i)
}

pub fn rbrace_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::RBrace, char('}'))(i)
}

pub fn lbracket_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::LBracket, char('['))(i)
}

pub fn rbracket_punctuation(i: &str) -> IResult<&str, Token> {
    value(Token::RBracket, char(']'))(i)
}

pub fn lex_punctuations(i: &str) -> IResult<&str, Token> {
    alt((
        comma_punctuation,
        semicolon_punctuation,
        lparen_punctuation,
        rparen_punctuation,
        lbrace_punctuation,
        rbrace_punctuation,
        lbracket_punctuation,
        rbracket_punctuation,
    ))(i)
}

pub fn parse_str<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, &'a str, E> {
    escaped(alphanumeric1, '\\', one_of("\"n\\"))(i)
}

pub fn string<'a, E: ParseError<&'a str> + ContextError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, &'a str, E> {
    context(
        "string",
        preceded(char('\"'), cut(terminated(parse_str, char('\"')))),
    )(i)
}

pub fn lex_string(i: &str) -> IResult<&str, Token> {
    map(string, |s| Token::StringLiteral(s.to_owned()))(i)
}

pub fn lex_bool(i: &str) -> IResult<&str, Token> {
    map(
        alt((value(true, tag("true")), value(false, tag("false")))),
        Token::BoolLiteral,
    )(i)
}

pub fn lex_ident(i: &str) -> IResult<&str, Token> {
    preceded(
        peek(satisfy(|c: char| c.is_ascii_alphabetic())),
        map(alphanumeric1, |rest: &str| Token::Ident(rest.to_owned())),
    )(i)
}

pub fn lex_integer(i: &str) -> IResult<&str, Token> {
    map(map_res(digit1, FromStr::from_str), Token::IntLiteral)(i)
}

pub fn lex_invalid(i: &str) -> IResult<&str, Token> {
    map(anychar, Token::Illegal)(i)
}

pub fn lex_token(i: &str) -> IResult<&str, Token> {
    alt((
        lex_operator,
        lex_punctuations,
        lex_string,
        lex_bool,
        lex_ident,
        lex_integer,
        lex_invalid,
    ))(i)
}

pub fn lex_tokens(i: &str) -> IResult<&str, Vec<Token>> {
    many0(preceded(multispace0, lex_token))(i)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_lexer1() {
        let input = "( ) {  } ,  ;";
        let (_, result) = lex_tokens(input).unwrap();

        let expected_results = vec![
            Token::LParen,
            Token::RParen,
            Token::LBrace,
            Token::RBrace,
            Token::Comma,
            Token::SemiColon,
        ];

        assert_eq!(result, expected_results);

        let input = "test; true; false; -6, 0,7";
        let (_, result) = lex_tokens(input).unwrap();

        let expected_results = vec![
            Token::Ident("test".to_owned()),
            Token::SemiColon,
            Token::BoolLiteral(true),
            Token::SemiColon,
            Token::BoolLiteral(false),
            Token::SemiColon,
            Token::Minus,
            Token::IntLiteral(6),
            Token::Comma,
            Token::IntLiteral(0),
            Token::Comma,
            Token::IntLiteral(7),
        ];

        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_lexer2() {
        let input = r#"translate([3,2,1]) {
            cube([1,2,3],5);
        }"#;
        let (_, result) = lex_tokens(input).unwrap();

        let expected_results = vec![
            Token::Ident("translate".to_owned()),
            Token::LParen,
            Token::LBracket,
            Token::IntLiteral(3),
            Token::Comma,
            Token::IntLiteral(2),
            Token::Comma,
            Token::IntLiteral(1),
            Token::RBracket,
            Token::RParen,
            Token::LBrace,
            Token::Ident("cube".to_owned()),
            Token::LParen,
            Token::LBracket,
            Token::IntLiteral(1),
            Token::Comma,
            Token::IntLiteral(2),
            Token::Comma,
            Token::IntLiteral(3),
            Token::RBracket,
            Token::Comma,
            Token::IntLiteral(5),
            Token::RParen,
            Token::SemiColon,
            Token::RBrace,
        ];

        assert_eq!(result, expected_results);
    }
}
